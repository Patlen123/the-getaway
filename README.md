# The Getaway (Legacy)

A CS-230 (SU) Project from 2020/21. Legacy build stored for archival, you can find the actively updated build here: https://gitlab.com/Patlen123/the-getaway-le

**Permissions:**

    None of this code is mine, it's just here for archival so I can't give permission for anything

**Some notes on running:**

_To run without compiling_

    Set "Assets" as resources root and "Source" as sources root

_To get the font correct:_ 

    Make sure that the filepath to the program doesn't include spaces


**Credits**

_The Getaway (Legacy):_

    Joshua Oladitan
    Atif Ishaq
    Christian Sanger
    David Langmaid
    George Sanger
    Brandon Chan
    James Sam
    Daniel James Ortega

 
